# SDL Utilities

These are utilities I have developed for working with the SDL C interface (or other C interfaces), these can also be used as general utilities. So far there is one utility: `Resource`. `Resource` is a *non-const-correct* runtime implmentation of borrow mechanichs, like from Rust or somewhat like [UC++](https://bitbucket.org/ChristopherGreeley/ucc/src/master/). If made const-correct in the future, it will allow for shared immutable state. Right now it only accomidates do unique mutable state.

# Usage
The reason this works with C is because it allows you to specify how something is created and destroyed, and what happens when something goes wrong.

```C++
template< auto ConstructFunctionConstantParameter,  
        auto DestructFunctionConstantParameter, 
        auto ErrorFunction >
struct Resource //...
```

The type of data the pointer will hold is determined by the return type of `ConstructFunctionConstantParameter`. The `Resource` will then manage the lifetime of the data by allocating the data with `ConstructFunctionConstantParameter` and performing clean up with `DestructFunctionConstantParameter`. Parameters for the `ConstructFunctionConstantParameter` function can either be passed to the constructor, or can be specified at compile time with the partial specilization

```C++
template< auto ConstructFunctionConstantParameter,  
        auto DestructFunctionConstantParameter, 
        auto ErrorFunction, 
        auto... ConstructionFuntionArgumentsConstantParameters >
struct Resource //...
```

It is reccomended to create a `typename` for each type which will be managed by `Resource`. For example

```C++
using TestResourcePointer = SDLUtility::Resource< CreateTestResource, 
        DestroyTestResource, 
        SDLUtility::Assert >;
```

Just to mention `Assert`, and `Throw` are provided as default error functions, `Assert` just calls `assert` and `Throw` throws an `std::exception`.

The error function is called whenever a `Resource` tries to call `->`, `Swap`, `=`, or be passed to another `Resource` when it does not own the data it points too. Apon creation, a `Resource` must own data, it then transfers ownership of the data when it is passed too a function, or is copied (either by constructor or `=`, everything calls `Swap` under the hood) or moved. To get a resource back from a function it was passed too, it must be returned from, essentially, `Resource` follows the philosophy that "shared mutable state is the root of all evil" (somewhat), so it allows for mutable but not shared state. In the future, if made const-correct, it will allow for shared but not mutable state. However I would like to prioritize my other library [BorrowPlusPlus](https://bitbucket.org/ChristopherGreeley/borrowplusplus/src) first where I would like to make `static_assert` catch the error, rather than an `ErrorFunction` at runtime, so one can gaurentee safe execution, get better diagnostics, and have no need of debugging, also it could be "constexpred" 


![Alt text](https://leighjohnston.files.wordpress.com/2019/10/3d2ltj1.jpg)
