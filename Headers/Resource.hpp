#include <type_traits>
#include <cstdint>
#include <tuple>
#include <assert.h>

#define SDL_UTILITY_UUID_d7e033e6_aab7_4a4d_97c2_ae2e755c2fe4 d7e033e6_aab7_4a4d_97c2_ae2e755c2fe4

#ifndef SDL_UTILITY_RESOURCE_HEADER_HPP_d7e033e6_aab7_4a4d_97c2_ae2e755c2fe4
#define SDL_UTILITY_RESOURCE_HEADER_HPP_d7e033e6_aab7_4a4d_97c2_ae2e755c2fe4
    #ifdef NDEBUG
        #define NDEBUG_d7e033e6_aab7_4a4d_97c2_ae2e755c2fe4
        #undef NDEBUG
    #endif

    namespace SDLUtility
    {
        void Assert( bool sentry )  {
            assert( sentry );
        }

        void Throw( bool shouldThrow ) {
            if( shouldThrow == false )
                throw std::exception{ "Bad access to borrowed pointer" };
        }
        
        template< typename ReturnTypeParameter, typename... ArgumentTypesParameter >
        struct FunctionProperties
        {
            using ReturnType = ReturnTypeParameter;
            using ArgumentTypes = std::tuple< ArgumentTypesParameter... >;
            constexpr static std::size_t NumberOfArguments = 
                    ( sizeof...( ArgumentTypesParameter ) );
            constexpr explicit FunctionProperties( 
                    ReturnTypeParameter(*)( ArgumentTypesParameter... ) ) {}
        };

        template< typename PointerTypeParameter >
        concept PointerConcept = std::is_pointer< PointerTypeParameter >::value;

        template< PointerConcept PointerTypeConstantParameter > 
        using ResourcePointerTypeChecker = PointerTypeConstantParameter;

        template< auto ConstructFunctionConstantParameter >
        using ResoucePointerType = ResourcePointerTypeChecker< 
                typename decltype( FunctionProperties( 
                        ConstructFunctionConstantParameter ) )::ReturnType >;

        template< auto ConstructFunctionConstantParameter,  
                auto DestructFunctionConstantParameter, 
                auto ErrorFunction >
        struct ResourceBase
        {
            using PointerType = ResoucePointerType< 
                        ConstructFunctionConstantParameter >;
            using RefrenceType = typename std::remove_pointer< PointerType >::type&;
            RefrenceType GetResourcePointer() {
                ErrorFunction( owns == true );
                return *resourcePointer;
            }
            operator RefrenceType() {
                return GetResourcePointer();
            }
            ResourceBase< ConstructFunctionConstantParameter, 
                    DestructFunctionConstantParameter, 
                    ErrorFunction >&
            Swap( ResourceBase< ConstructFunctionConstantParameter, 
                    DestructFunctionConstantParameter, 
                    ErrorFunction >& other )
            {
                ErrorFunction( owns == true );
                resourcePointer = other.resourcePointer;
                other.owns = false;
                return *this;
            }
            ResourceBase< ConstructFunctionConstantParameter, 
                    DestructFunctionConstantParameter, 
                    ErrorFunction >&
            Swap( ResourceBase< ConstructFunctionConstantParameter, 
                    DestructFunctionConstantParameter, 
                    ErrorFunction >&& other )
            {
                ErrorFunction( owns == true );
                resourcePointer = other.resourcePointer;
                other.owns = false;
                return *this;
            }
            protected: 
                bool owns = true;
                PointerType resourcePointer;
        };

        template< auto ConstructFunctionConstantParameter, 
                auto DestructFunctionConstantParameter, 
                auto ErrorFunction >
        struct StructureTypeResourcePointer : public ResourceBase< 
                ConstructFunctionConstantParameter, 
                DestructFunctionConstantParameter, 
                ErrorFunction >
        {
            using BaseType = ResourceBase< ConstructFunctionConstantParameter, 
                DestructFunctionConstantParameter, 
                ErrorFunction >;
            ResoucePointerType< 
                    ConstructFunctionConstantParameter > operator->() {
                ErrorFunction( BaseType::owns == true );
                return BaseType::resourcePointer;
            }
        };

        template< auto ConstructFunctionConstantParameter, 
                auto DestructFunctionConstantParameter, 
                auto ErrorFunction >
        using ObtainBase = typename std::conditional< 
                std::is_class< 
                        typename std::remove_pointer< 
                                ResoucePointerType< 
                                        ConstructFunctionConstantParameter 
                                > >::type 
                >::value, 
                StructureTypeResourcePointer< 
                        ConstructFunctionConstantParameter, 
                        DestructFunctionConstantParameter, 
                        ErrorFunction >, 
                ResourceBase< 
                        ConstructFunctionConstantParameter, 
                        DestructFunctionConstantParameter, 
                        ErrorFunction > >::type;

        template< auto ConstructFunctionConstantParameter,  
                auto DestructFunctionConstantParameter, 
                auto ErrorFunction, 
                auto... ConstructionFuntionArgumentsConstantParameters >
        struct Resource : public ObtainBase< ConstructFunctionConstantParameter, 
                DestructFunctionConstantParameter, ErrorFunction >
        {
            using BaseType = ObtainBase< ConstructFunctionConstantParameter, 
                DestructFunctionConstantParameter, ErrorFunction >;
            constexpr Resource() : BaseType::resourcePointer( 
                    ConstructFunctionConstantParameter( std::forward< decltype( 
                            ConstructionFuntionArgumentsConstantParameters ) >( 
                            ConstructionFuntionArgumentsConstantParameters )... ) ) {}
            constexpr Resource( Resource& other ) {
                BaseType::Swap( other );
            }
            constexpr Resource( Resource&& other ) {
                BaseType::Swap( other );
            }
            ~Resource() {
                if( BaseType::owns == true )
                    DestructFunctionConstantParameter( BaseType::resourcePointer );
            }
            Resource& operator=( Resource& other ) {        
                return static_cast< decltype( *this )& >( BaseType::Swap( other ) );
            }
            Resource& operator=( Resource&& other ) {
                return static_cast< decltype( *this )& >( BaseType::Swap( other ) );
            }
        };

        template< auto ConstructFunctionConstantParameter,  
                auto DestructFunctionConstantParameter, 
                auto ErrorFunction >
        struct Resource< ConstructFunctionConstantParameter, 
                DestructFunctionConstantParameter, 
                ErrorFunction > : 
                public ObtainBase< ConstructFunctionConstantParameter, 
                        DestructFunctionConstantParameter, 
                        ErrorFunction >
        {
            using BaseType = ObtainBase< ConstructFunctionConstantParameter, 
                DestructFunctionConstantParameter, 
                ErrorFunction >;
            template< typename... ConstructionFuntionArguments >
            constexpr Resource( ConstructionFuntionArguments... arguments ) {
                    BaseType::resourcePointer = ConstructFunctionConstantParameter( 
                            std::forward< ConstructionFuntionArguments >( arguments )... ); }
            constexpr Resource( Resource& other ) {
                BaseType::Swap( other );
            }
            constexpr Resource( Resource&& other ) {
                BaseType::Swap( other );
            }
            ~Resource() {
                if( BaseType::owns == true )
                    DestructFunctionConstantParameter( BaseType::resourcePointer );
            }
            Resource& operator=( Resource& other ) {
                return static_cast< decltype( *this )& >( BaseType::Swap( other ) );
            }
            Resource& operator=( Resource&& other ) {
                return static_cast< decltype( *this )& >( BaseType::Swap( other ) );
            }
        };
    }
    #ifdef NDEBUG_d7e033e6_aab7_4a4d_97c2_ae2e755c2fe4
        #define NDEBUG
    #endif
#endif

