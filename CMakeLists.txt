cmake_minimum_required(VERSION 3.10)

set(CMAKE_CXX_STANDARD 20)

project(SDLUtilities)

find_package(SDL2 CONFIG REQUIRED)
find_package(EnTT CONFIG REQUIRED)
find_package(Catch2 CONFIG REQUIRED)
find_package(sdl2-image CONFIG REQUIRED)

file(GLOB_RECURSE SOURCES RELATIVE "${CMAKE_SOURCE_DIR}" "Source/*.cpp")
file(GLOB_RECURSE TESTS RELATIVE "${CMAKE_SOURCE_DIR}" "Tests/*.cpp")

add_executable(SDLUtilities ${SOURCES} ${TESTS})

target_include_directories(SDLUtilities PUBLIC 
		"${PROJECT_SOURCE_DIR}/Headers" )

target_link_libraries(SDLUtilities PRIVATE SDL2::SDL2 SDL2::SDL2main)
target_link_libraries(SDLUtilities PRIVATE EnTT::EnTT)
target_link_libraries(SDLUtilities PRIVATE Catch2::Catch2)
target_link_libraries(SDLUtilities PRIVATE SDL2::SDL2_image)

