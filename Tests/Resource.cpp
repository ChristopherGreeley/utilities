#include <csignal>

#include <atomic>
#include <string>
#include <random>

#define CATCH_CONFIG_MAIN
#include <catch.hpp>

#include <Resource.hpp>

struct TestResource {
    std::string testData;
};

TestResource* CreateTestResource( std::string testData );

void DestroyTestResource( TestResource* toDestroy );

using TestResourcePointer = SDLUtility::Resource< CreateTestResource, 
        DestroyTestResource, 
        SDLUtility::Throw >;

const static std::string TestStringConstant = "Hello World";
const static std::string DoubleTestStringConstant = ( TestStringConstant + TestStringConstant );

std::string RepeatString( std::string what, int amount );

void TestNoModifyNoReturnFunction( TestResourcePointer resource );
TestResourcePointer TestFunction( TestResourcePointer resource );
void TestNoReturnFunction( TestResourcePointer resource );
TestResourcePointer TestNoModifyFunction( TestResourcePointer resource );
void ReadToDepth( TestResourcePointer resource, int depth );
void ModifyToDepth( TestResourcePointer resource, int depth );
TestResourcePointer ReturnToDepth( TestResourcePointer resource, int depth );
TestResourcePointer ReturnModifyToDepth( TestResourcePointer resource, int depth );
TestResourcePointer ReturnDoubleModifyToDepth( TestResourcePointer resource, int depth );
void DepthTest( int depth );

TEST_CASE( "Resource type testnig", "[resource]" )
{
    SECTION( "Create and destroy a resource" ) {
        TestResourcePointer testPointer( "Hello World" );
    }
    SECTION( "Create a resource, read data, and destroy resource" ) {
        TestResourcePointer testPointer( TestStringConstant );
        REQUIRE( testPointer->testData == TestStringConstant );
    }
    SECTION( "Parallel allocation" )
    {
        WHEN( "Parent is ouside a block, borrower inside a block" )
        {
            TestResourcePointer testPointer( TestStringConstant );
            REQUIRE_NOTHROW( testPointer->testData );
            {
                TestResourcePointer nextPointer = testPointer;
                REQUIRE_NOTHROW( nextPointer->testData );
                REQUIRE_THROWS_AS( testPointer->testData, std::exception );
            }
            REQUIRE_THROWS_AS( testPointer->testData, std::exception );
        }
        WHEN( "Parent and borrower are inside the same block" )
        {
            TestResourcePointer testPointer( TestStringConstant );
            REQUIRE_NOTHROW( testPointer->testData );
            TestResourcePointer nextPointer = testPointer;
            REQUIRE_NOTHROW( nextPointer->testData );
            REQUIRE_THROWS_AS( testPointer->testData, std::exception );
        }
    }
    SECTION( "Passing a resource to functions" )
    {
        WHEN( "The resource is not returned and not modified" )
        {
            TestResourcePointer testPointer( TestStringConstant );
            TestNoModifyNoReturnFunction( testPointer );
            THEN( "The pointer is derfrerenced" ) {
                REQUIRE_THROWS_AS( testPointer->testData, std::exception );
            }
        }
        WHEN( "The resource is returned and not modified" )
        {
            TestResourcePointer testPointer( TestStringConstant );
            TestNoModifyFunction( testPointer );
            THEN( "The pointer is derfrerenced" ) {
                REQUIRE_THROWS_AS( testPointer->testData, std::exception );
            }
        }
        WHEN( "The resource is returned, set to a local, and not modified" )
        {
            TestResourcePointer testPointer( TestStringConstant );
            TestResourcePointer nextPointer = TestNoModifyFunction( testPointer );
            THEN( "The new assigned pointer is derfrerenced" ) {
                REQUIRE_NOTHROW( nextPointer->testData );
            }
            THEN( "The old pointer is derferenced" ) {
                REQUIRE_THROWS_AS( testPointer->testData, std::exception );
            }
        }
        WHEN( "The resource is modified but not returned" )
        {
            TestResourcePointer testPointer( TestStringConstant );
            REQUIRE_NOTHROW( TestNoReturnFunction( testPointer ) );
            THEN( "The pointer is derfrerenced" ) {
                REQUIRE_THROWS_AS( testPointer->testData, std::exception );
            }
        }
        WHEN( "The resource is modified, and is returned" )
        {
            TestResourcePointer testPointer( TestStringConstant );
            REQUIRE_NOTHROW( TestFunction( testPointer ) );
            THEN( "The pointer is derfrerenced" ) {
                REQUIRE_THROWS_AS( testPointer->testData, std::exception );
            }
        }
        WHEN( "The resource is returned, set to a local, and is modified" )
        {
            TestResourcePointer testPointer( TestStringConstant );
            TestResourcePointer nextPointer = TestFunction( testPointer );
            THEN( "The new assigned pointer is derfrerenced" ) {
                REQUIRE_NOTHROW( nextPointer->testData );
                REQUIRE( nextPointer->testData == DoubleTestStringConstant );
            }
            THEN( "The old pointer is derferenced" ) {
                REQUIRE_THROWS_AS( testPointer->testData, std::exception );
            }
        }
    }
    SECTION( "Depth testing" )
    {
        /*
            ReadToDepth( TestResourcePointer resource, int depth )
            ModifyToDepth( TestResourcePointer resource, int depth )
            ReturnToDepth( TestResourcePointer resource, int depth )
            ReturnModifyToDepth( TestResourcePointer resource, int depth )
            ReturnDoubleModifyToDepth( TestResourcePointer resource, int depth )
        */
        for( int i = 0; i < 25; ++i )
        {
            WHEN( "The depth is " + std::to_string( i ) ) {
                DepthTest( i );
            }
        }
        std::uniform_int_distribution< int > uniformDistribution( 0, 126 );
        for( int i = 0; i < 10; ++i )
        {
            WHEN( "The randomly selected depth is " + std::to_string( i ) ) {
                DepthTest( i );
            }
        }
    }
}

TestResource* CreateTestResource( std::string testData ) {
    return new TestResource{ .testData = testData };
}

void DestroyTestResource( TestResource* toDestroy ) {
    delete toDestroy;
}

void TestNoModifyNoReturnFunction( TestResourcePointer resource ) {
    REQUIRE( resource->testData == TestStringConstant );
}

std::string RepeatString( std::string what, int amount )
{
    std::string test = "";
    for( int i = 0; i < amount; ++i )
        test += what;
    return test;
}

TestResourcePointer TestNoModifyFunction( TestResourcePointer resource ) {
    REQUIRE( resource->testData == TestStringConstant );
    return resource;
}

TestResourcePointer TestFunction( TestResourcePointer resource )
{
    REQUIRE( resource->testData == TestStringConstant );
    resource->testData = DoubleTestStringConstant;
    REQUIRE( resource->testData == DoubleTestStringConstant );
    return resource;
}

void TestNoReturnFunction( TestResourcePointer resource )
{
    REQUIRE( resource->testData == TestStringConstant );
    resource->testData = DoubleTestStringConstant;
    REQUIRE( resource->testData == DoubleTestStringConstant );
}

void ReadToDepth( TestResourcePointer resource, int depth )
{
    REQUIRE_NOTHROW( resource->testData );
    if( depth > 0 ) {
        ReadToDepth( resource, depth - 1 );
        REQUIRE_THROWS_AS( resource->testData, std::exception );
    }
}

void ModifyToDepth( TestResourcePointer resource, int depth )
{
    auto testFunction = [ & ]() {
        resource->testData = ( resource->testData + TestStringConstant );
    };
    REQUIRE_NOTHROW( testFunction() );
    if( depth > 0 ) {
        ModifyToDepth( resource, depth - 1 );
        REQUIRE_THROWS_AS( testFunction(), std::exception );
    }
}

TestResourcePointer ReturnToDepth( TestResourcePointer resource, int depth )
{
    REQUIRE_NOTHROW( resource->testData );
    if( depth > 0 )
    {
        TestResourcePointer nextPointer = ReturnToDepth( resource, depth - 1 );
        REQUIRE_THROWS_AS( resource->testData, std::exception );
        REQUIRE_NOTHROW( nextPointer->testData );
        return nextPointer;
    }
    return resource;
}

TestResourcePointer ReturnModifyToDepth( TestResourcePointer resource, int depth )
{
    auto testFunction = [ & ]() {
        resource->testData = ( resource->testData + TestStringConstant );
    };
    REQUIRE_NOTHROW( testFunction() );
    const std::string BeforeConstant = resource->testData;
    if( depth > 0 )
    {
        TestResourcePointer nextPointer = ReturnModifyToDepth( resource, depth - 1 );
        REQUIRE_THROWS_AS( testFunction(), std::exception );
        REQUIRE_NOTHROW( nextPointer->testData );
        REQUIRE( nextPointer->testData == ( BeforeConstant + 
                RepeatString( TestStringConstant, depth ) ) );
        return nextPointer;
    }
    return resource;
}

TestResourcePointer ReturnDoubleModifyToDepth( TestResourcePointer resource, int depth )
{
    REQUIRE_NOTHROW( resource->testData = ( resource->testData + TestStringConstant ) );
    const std::string BeforeConstant = resource->testData;
    if( depth > 0 )
    {
        TestResourcePointer nextPointer = ReturnDoubleModifyToDepth( resource, depth - 1 );
        REQUIRE_THROWS_AS( resource->testData, std::exception );
        REQUIRE_NOTHROW( nextPointer->testData = ( nextPointer->testData + TestStringConstant ) );
        REQUIRE( nextPointer->testData == ( BeforeConstant + 
                RepeatString( TestStringConstant, depth * 2 ) ) );
        return nextPointer;
    }
    return resource;
}


void DepthTest( int depth )
{
    auto RecurseAndCapture = [ & ]( std::string action, 
            TestResourcePointer toRecurse( TestResourcePointer, int ) )
    {
        THEN( action + " to depth" ) {
            TestResourcePointer resource( TestStringConstant );
            REQUIRE_NOTHROW( toRecurse( resource, depth ) );
        }
        THEN( action + "to depth and capture" )
        {
            TestResourcePointer resource( TestStringConstant );
            REQUIRE_NOTHROW( [ & ]() {
                TestResourcePointer nextPointer = toRecurse( resource, depth );
                REQUIRE( nextPointer->testData == RepeatString( TestStringConstant, depth + 1 ) );
            } );
        }
    };
    THEN( "Read to depth" ) {
        TestResourcePointer resource( TestStringConstant );
        REQUIRE_NOTHROW( ReadToDepth( resource, depth ) );
    }
    THEN( "Modify to depth" ) {
        TestResourcePointer resource( TestStringConstant );
        REQUIRE_NOTHROW( ModifyToDepth( resource, depth ) );
    }
    RecurseAndCapture( "Return", ReturnToDepth );
    RecurseAndCapture( "Return and modify", ReturnModifyToDepth );
    RecurseAndCapture( "Modify return/recurse and modify return", ReturnDoubleModifyToDepth );
}
